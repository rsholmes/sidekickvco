# sidekickvco

## sidekickvco_mainPCB

![PCB front](Docs/Layout/sidekickvco_mainPCB/sidekickvco_mainPCB_front.svg)
![PCB back](Docs/Layout/sidekickvco_mainPCB/sidekickvco_mainPCB_back.svg)

## sidekickvco_switchPCB

![PCB front](Docs/Layout/sidekickvco_switchPCB/sidekickvco_switchPCB_front.svg)
![PCB back](Docs/Layout/sidekickvco_switchPCB/sidekickvco_switchPCB_back.svg)

